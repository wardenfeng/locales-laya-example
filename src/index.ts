let result: {
    localesObject: {
        [key: string]: string;
    };
    currentLanguage: string;
}
let languageIndex = 0;

async function useLanguage() {
    await initLanguage();
    updateLanguage();
}

async function initLanguage() {
    if (result) return;

    result = await locales.getLocalesObject({
        isCollect: true,
        localesDir: "locales",
        languages: ["zh", "en"],
        readJsonFile: async (path) => {

            if (path === 'locales/zh.json') {
                return {
                    '切换语言': 'switch the language',
                    '点我赋值': '点我赋值',
                    'Hello LayaAir': '中文',
                    'item': '项',
                    'This is a Label': '这是一个标签',
                    'this is a list': '这是一个列表',
                };
            }

            if (path === 'locales/en.json') {
                return {
                    '切换语言': '切换语言',
                    '点我赋值': 'Click on the assignment',
                    'Hello LayaAir': 'Hello LayaAir',
                    'item': 'item',
                    'This is a Label': 'This is a Label',
                    'this is a list': 'this is a list',
                };
            }

            console.log(`readJsonFile ${path} 失败！`)
        },
        writeJsonFile: async (path, object) => {
            console.log(`writeJsonFile ${path}`, object);
        },
        extract: ["(item) [\\d]+"],
    })

    Laya.Text.langPacks = result.localesObject;
}

async function changeLanguge() {
    await initLanguage();
    result.currentLanguage = ["zh", "en"][(++languageIndex) % 2];
    updateLanguage();
}

function updateLanguage() {

    console.log(`使用语言包更新文本`);

    locales_laya.updateLanguage(Laya.stage);
}


